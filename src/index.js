const Koa = require('koa');
const KoaRouter = require('koa-router');
const database = require('./database');

const app = new Koa();
const router = new KoaRouter();

router.get('/', async ctx => {
  ctx.body = {
    description: 'Atlas of Thrones API',
    version: 'v1.0.0',
  };
});

router.get('/locations/:type', async ctx => {
  const { type } = ctx.params;
  const results = await database.getLocations(type);

  if (results.length === 0) {
    ctx.throw(404);
  }

  const locations = results.map(row => {
      const geojson = JSON.parse(row.st_asgeojson);
      geojson.properties = {
        name: row.name,
        type: row.type,
        id: row.gid,
      };
      return geojson;
  });

  ctx.body = locations;
});

router.get('/kingdoms', async ctx => {
  const results = await database.getKingdomBoundaries();

  if (results.length === 0) {
    ctx.throw(404);
  }

  const boundaries = results.map(row => {
      let geojson = JSON.parse(row.st_asgeojson);
      geojson.properties = {
        name: row.name,
        id: row.gid,
      };
      return geojson;
  });

  ctx.body = boundaries;
});

app.use(router.routes()).use(router.allowedMethods());
app.listen(3000);
