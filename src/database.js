const Knex = require('knex');
const config = require('../knexfile');

const knex = Knex(config);

module.exports = {
  getLocations: async type => knex('locations')
    .select(knex.raw('ST_AsGeoJSON(geog), name, type, gid'))
    .where(knex.raw('LOWER("type") = ?', type.toLowerCase())),

  getKingdomBoundaries: async () => knex('kingdoms')
    .select(knex.raw('ST_AsGeoJSON(geog), name, gid'))
};
