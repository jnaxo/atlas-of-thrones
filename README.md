# Atlas of Thrones - Postgis Example
Game of Thrones map with PostGIS and [Koajs](https://koajs.com/) framework for node.js.

## Requerimients
- Docker and Docker compose

## Getting Started
Run
```
$ npm run init-containers
```

## Original Post
- https://blog.patricktriest.com/game-of-thrones-map-node-postgres-redis/