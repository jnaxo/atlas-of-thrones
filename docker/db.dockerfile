FROM mdillon/postgis:9.4-alpine

ADD database/atlas_of_thrones.sql /docker-entrypoint-initdb.d/
